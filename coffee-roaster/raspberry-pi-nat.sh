#!/bin/bash

#ifconfig eth0:1 192.168.0.1 netmask 255.255.255.0
ifconfig eth0:1 10.22.7.1 netmask 255.255.255.0

echo 1 > /proc/sys/net/ipv4/ip_forward

/sbin/iptables -t nat -A POSTROUTING -o wlan0 -j MASQUERADE
/sbin/iptables -A FORWARD -i eth0 -o wlan0 -m state --state RELATED,ESTABLISHED -j ACCEPT
/sbin/iptables -A FORWARD -i wlan0 -o eth0 -j ACCEPT
