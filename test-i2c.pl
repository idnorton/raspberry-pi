#!/usr/bin/perl

use strict;
use warnings;

use HiPi::Device::I2C; # qw( ioctl );

my $i2c = HiPi::Device::I2C->new('/dev/i2c-1');

$i2c->select_address( 0x20 );

my $regadress;
my @bytes;
my $numbytes;

$i2c->smbus_write($regadress, @bytes);

@bytes = $i2c->smbus_read($regadress, $numbytes);
