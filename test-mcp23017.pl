#!/usr/bin/perl

use strict;
use warnings;

use HiPi::Constant qw( :mcp23017 );
use HiPi::MCP23017;

#my $mcp = HiPi::MCP23017->new;

my $mcp = HiPi::MCP23017->new({
    i2caddress   => 0x20,
});

# set all pins on port A as output
# 0 for output
# 1 for input
#$mcp->write_register_bits('IODIRA', (0,0,0,0,0,0,0,0));

# Tell the library to use IOCON.SEQ
#
# bit 7-6 Unimplemented: Read as ‘0’.
# bit 5 SEQOP: Sequential Operation mode bit.
#       1 = Sequential operation disabled, address pointer does not increment.
#       0 = Sequential operation enabled, address pointer increments.
# bit 4 DISSLW: Slew Rate control bit for SDA output.
#       1 = Slew rate disabled.
#       0 = Slew rate enabled.
# bit 3 HAEN: Hardware Address Enable bit (MCP23S08 only).
#      Address pins are always enabled on MCP23008.
#       1 = Enables the MCP23S08 address pins.
#       0 = Disables the MCP23S08 address pins.
# bit 2 ODR: This bit configures the INT pin as an open-drain output.
#       1 = Open-drain output (overrides the INTPOL bit).
#       0 = Active driver output (INTPOL bit sets the polarity).
# bit 1 INTPOL: This bit sets the polarity of the INT output pin.
#       1 = Active-high.
#       0 = Active-low.
# bit 0 
#       1 - required for MCP23008
#
# Set the register bits as detailed above
$mcp->write_register_bits('IOCON', (0,0,1,1,1,1,1,1));

# Start with an array of zeros
#my @reset_bits = (0,0,0,0,0,0,0,0);

# Write all zeros out to the GPIO
#$mcp->write_register_bits('GPIOA', @reset_bits);

# Write all ones out to the GPIO
#$mcp->write_register_bits('GPIOA', (1,0,1,0,1,0,1,0));

#exit;

#my @bits = (1,1,1,1,1,1,1,1);

# set level of port A pin 7 high
my @bits = $mcp->read_register_bits('GPIOA');

use Data::Dumper;
print(Dumper(@bits) . "\n");

if($bits[1] == 1) {
    $bits[1] = 0;
}
else {
    $bits[1] = 1;
}

$mcp->write_register_bits('GPIOA', @bits);

# check level of port A pin 3
@bits = $mcp->read_register_bits('GPIOA');
#my $val = $bits[3];

print(Dumper(@bits));
